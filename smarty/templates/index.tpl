<html>
    {include file="header.tpl"}

    <body>


        <div class="container">

            <div class="row">
                <label class="col-md-1 col-lg-offset-4 ">Cedula</label>                       
                <input type="text" id="cedula" class="col-md-2">
            </div>
            <br>
            <div class="row">
                <label class="col-md-1 col-lg-offset-4">Nombre</label>                       
                <input type="text" id="nombre" class="col-md-2">
            </div>
            <br>
            <div class="row">
                <label class="col-md-1 col-lg-offset-4">Correo</label>                       
                <input type="text" id="correo" class="col-md-2">
            </div>
            <br>
            <div class="row">
                <label class="col-md-1 col-lg-offset-4">Edad</label>                       
                <input type="text" id="edad" class="col-md-2">
            </div>
            <br>
            <div class="row">
                <label class="col-md-1 col-lg-offset-4">Genero</label>                       
                <select class="col-md-2" id="genero">
                    <option value="1">Masculino</option>
                    <option value="2">Femenino</option>
                </select>
            </div>
            <br>
            <div class="row">
                <button class="col-md-1 col-lg-offset-5" id="crear" > Crear </button>
            </div>

        </div>

        <div id="resultado"> aqui va el resultado</div>

        <table class="table table-striped" id="tabla">
            <thead>
                <tr>
                    <th>Cedula</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Edad</th>
                    <th>Genero</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

        <div id="dialog" title="Basic dialog">
            
        </div>

    </body>

    {include file = "footer.tpl"}
</html>
