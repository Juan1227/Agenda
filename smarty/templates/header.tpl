

    <head>
        <meta charset="UTF-8">
        <title>Agenda</title>

        <link rel="stylesheet" type="text/css" href="lib/DataTables-1.10.15/media/css/dataTables.bootstrap.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="lib/bootstrap-3.3.7-dist/css/bootstrap.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="lib/jquery-ui-1.12.1.custom/jquery-ui.css" media="screen" />
        <script type="text/javascript" src="lib/jQuery/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="lib/DataTables-1.10.15/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="lib/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
        <script type="text/javascript" src="js/validarUsuario.js"></script>
        <script type="text/javascript" src="lib/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
        
    </head>
    