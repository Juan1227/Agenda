<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_datos_arreglo($arreglo,$valor="",$caracter="|") 
{ 
   	$nuevo_arreglo=explode($caracter,$arreglo);
	$cantidad=0;
	for($i=0;$i<count($nuevo_arreglo);$i++)
	{
		if($nuevo_arreglo[$i]==$valor)
		{
			$cantidad=$cantidad+1;
		}
	}

	return $cantidad;
} 
?>