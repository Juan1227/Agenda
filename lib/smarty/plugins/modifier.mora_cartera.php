<?php 
/** 
 * Smarty plugin 
 * @package Smarty 
 * @subpackage plugins 
 */ 


/** 
 * Smarty number_format modifier plugin 
 * 
 * Type:     modifier<br> 
 * Name:     number_format<br> 
 * Purpose:  returns a formatted number as of php number_format 
 * @author   ulyxes <zulisse at email dot it> 
 * @param string 
 * @param string 
 * @param string 
 * @param string 
 * @return string 
 */ 
function smarty_modifier_mora_cartera($valor=0, $dias=0,$dias_gracia=0,$interes) 
{ 
        //fechas
        $calculomora=0;
        $totalmora=0;
        $dias_mora=intval($dias)-intval($dias_gracia);
	  if($dias_mora>0)
        {
              $calculomora=$valor*($interes/100)/30*$dias_mora;
              $totalmora=round($calculomora);
        }
        return $totalmora;
} 
/* vim: set expandtab: */ 

?> 
