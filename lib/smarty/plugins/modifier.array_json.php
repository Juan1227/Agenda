<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_array_json($valor) 
{ 
    $arreglo=json_decode($valor,true );
    $arreglo_retorno=array();
    for($i=0;$i<count($arreglo);$i++)
    {
	$arreglo_retorno["valor"][]=$arreglo[$i]["valor"];
	$arreglo_retorno["nombre"][]=$arreglo[$i]["nombre"];
    }

    return $arreglo_retorno; 
} 
?>
