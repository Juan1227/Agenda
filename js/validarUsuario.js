$(document).ready(function () {
    var cedula, nombre, correo, edad, genero, usuarios, usuario;
    $("#crear").click(function () {
        cedula = $('#cedula').val();
        nombre = $('#nombre').val();
        correo = $('#correo').val();
        edad = $('#edad').val();
        genero = $('#genero').val();

        datos = {"cedula": cedula, "nombre": nombre, "correo": correo, "edad": edad, "genero": genero};
        $.ajax({
            method: "POST",
            url: "php/guardarUsuario.php",
            data: datos,
            success: function (data) {
                $("#resultado").html("exito " + data);
            }
        });


    });

    $.ajax({
        method: "POST",
        url: "php/listarUsuario.php",
        success: function (data) {
            usuarios = JSON.parse(data);

            var genero;
            for (var i in usuarios) {
                if (usuarios[i][4] == "1") {
                    genero = "Masculino";
                } else {
                    genero = "Femenino"
                }
                $('#tabla tbody').append(
                        "<tr><td>" + usuarios[i][0] + "</td>" +
                        "<td>" + usuarios[i][1] + "</td>" +
                        "<td>" + usuarios[i][2] + "</td>" +
                        "<td>" + usuarios[i][3] + "</td>" +
                        "<td>" + genero + "</td></tr>"
                        );
            }

            var dat = $('#tabla').DataTable();
            $('#tabla tbody').on('click', 'tr', function () {
                var data = dat.row(this).data();
                cargarDialogo(data[0]);
                ;
            });
        }
    });

    function cargarDialogo(cedulaSeleccionada) {
        $.ajax({
            method: "POST",
            url: "php/buscarUsuario.php",
            data: {cedula: cedulaSeleccionada},
            success: function (data) {
                usuario = JSON.parse(data);
                $("#dialog").dialog();
                $('#dialog').append(
                        "<p>fdd<p>"
                        );
            }
        });

    }

});

        